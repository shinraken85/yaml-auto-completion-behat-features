package pa.atkins;

import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.util.ProcessingContext;
import com.sun.corba.se.spi.ior.ObjectKey;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.plugins.cucumber.psi.GherkinLanguage;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class YamlCompletionContributor extends CompletionContributor {
    public YamlCompletionContributor() {
        /*if the parameter position is an xml attribute provide attributes using given xsd*/

        extend(CompletionType.BASIC,
            PlatformPatterns.psiElement().withLanguage(GherkinLanguage.INSTANCE),
            new CompletionProvider<CompletionParameters>() {
                public void addCompletions(@NotNull CompletionParameters parameters,
                                           ProcessingContext context,
                                           @NotNull CompletionResultSet resultSet) {

                    String yamlPath = parameters.getEditor().getProject().getBasePath().concat("/resource/placeholder/");
                    String lookup = resultSet.getPrefixMatcher().toString();
                    int index = lookup.lastIndexOf("{{");
                    String search = lookup.substring(index+2);

                    if (lookup.contains("{{element") || lookup.contains("{{data")) {

                        YamlProvider yamlProvider = new YamlProvider(yamlPath, search);
                        HashMap suggestions = yamlProvider.getSuggestions();
                        String[] splitSearch = search.split("\\.");
                        String lastSearch = splitSearch.length > 0 ? splitSearch[splitSearch.length-1] : "";
                        String suggestion;
                        String prefix = "";

                        for (Object key : suggestions.keySet()) {

                            suggestion = key.toString();

                            if (!lastSearch.isEmpty() && lastSearch != suggestion && suggestion.startsWith(lastSearch)) {
                                prefix = lastSearch;
                            }

                            resultSet.withPrefixMatcher(prefix).addElement(LookupElementBuilder.create(suggestion)
                                    .appendTailText("   " + suggestions.get(key), true).bold());
                        }
                    }
                }
            }
        );
    }
}
