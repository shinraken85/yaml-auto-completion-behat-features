package pa.atkins;

import java.io.*;
import java.util.*;
import org.yaml.snakeyaml.Yaml;

public class YamlProvider {

    private HashMap<String, String> suggestions = new HashMap<>();

    public YamlProvider(String yamlFilesPath, String search) {

        if (search.contains(".")) {
            search = search.replace(".", "/");
        }

        search = search.replace("'", "").replace(" ", "");

        if (!search.endsWith("/")) {
            search = search.concat("/");
        }

        String activePath = yamlFilesPath.concat(search);
        File baseFile = new File(activePath);
        File[] files = baseFile.listFiles();

        if (files != null) {
            for (File file : files) {
                getFileParameters(file);
            }
        } else {
            if (activePath.endsWith("/")) {
                activePath = activePath.substring(0, activePath.length() - 1) + ".yml";
                baseFile = new File(activePath);
                HashMap yamlParams;

                if (baseFile.exists()) {
                    addSuggestions(getYamlParameters(baseFile, ""));
                } else {
                    String path = "";
                    String yamlPath = "";
                    String previousPath = "";
                    Boolean defaultYamlParam = false;
                    String[] split = activePath.replaceFirst("/", "").replace(".yml", "").split("/");
                    baseFile = null;
                    Boolean addToYamlPath;
                    int i = 0;

                    for (String file : split) {
                        addToYamlPath = true;

                        if (i == 0) {
                            file = "/" + file;
                        }

                        if (!path.isEmpty()) {
                            previousPath = path;
                            path = path + "/" + file;
                        } else {
                            path = file;
                        }

                        File currentFile = new File(path);

                        if (!currentFile.exists() && !file.isEmpty()) {

                            if (!path.isEmpty() && baseFile == null) {
                                baseFile = new File(path.concat(".yml"));
                                addToYamlPath = false;


                                if (!baseFile.exists()) {
                                    baseFile = new File(previousPath.concat("/default.yml"));
                                    defaultYamlParam = true;
                                    addToYamlPath = true;
                                }
                            }

                            if (addToYamlPath) {
                                yamlPath = yamlPath + file + "/";
                            }

                        }

                        i++;
                    }

                    if (!yamlPath.isEmpty() && !path.isEmpty() && !defaultYamlParam) {
                        yamlParams = getYamlParameters(baseFile, yamlPath.substring(0, yamlPath.length() -1));

                        addSuggestions(yamlParams);
                    } else if (defaultYamlParam) {
                        HashMap yamlSuggestions = getYamlParameters(baseFile, yamlPath);

                        addSuggestions(yamlSuggestions);
                    }
                }
            }

        }



    }

    public void getFileParameters(File file) {
        if (file.getName().equals("default.yml")) {
            HashMap yamlSuggestions = getYamlParameters(file, "");
            addSuggestions(yamlSuggestions);
        } else {
            addSuggestion(file.getName().replace(".yml", ""), "");
        }
    }

    private HashMap<String, String> getYamlParameters(File file, String yamlPath){

        HashMap<String, String> params = new HashMap<>();

        try {
            Yaml yaml = new Yaml();

            Reader reader = new FileReader(file.getAbsoluteFile().toString());
            Map<String, Object> obj = yaml.load(reader);
            Map<String, Object> previousYamlObject = new LinkedHashMap<>();
            String key;
            String value;

            if (!obj.isEmpty()) {

                String[] splitYamlPath = yamlPath.split("/");
                if (!yamlPath.isEmpty()) {
                    for (int i = 0; i <= splitYamlPath.length; i++) {
                        if (i <= splitYamlPath.length-1) {
                            previousYamlObject = obj;
                            obj = getYamlLevel(obj, splitYamlPath[i]);

                            //Handle partial suggestions
                            if (obj.isEmpty() && i == splitYamlPath.length-1) {
                                obj = previousYamlObject;
                                for (Map.Entry<String, Object> entry : obj.entrySet()) {
                                    key = entry.getKey();
                                    value = entry.getValue() instanceof String ? entry.getValue().toString() : "";

                                    if (key.startsWith(splitYamlPath[i])) {
                                        params.put(key, value);
                                    }
                                }
                            }
                        }
                    }
                }

                for (Map.Entry<String, Object> entry : obj.entrySet()) {
                    key = entry.getKey();
                    value = entry.getValue() instanceof String ? entry.getValue().toString() : "";
                    params.put(key, value);
                }
            }
        } catch(Exception e) {
            params.put(e.getMessage(), "");
        }

        return params;
    }

    private LinkedHashMap getYamlLevel(Map<String, Object>  obj, String key) {
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        for (Map.Entry<String, Object> entry : obj.entrySet()) {
            if (entry.getValue() instanceof LinkedHashMap && key.equals(entry.getKey())) {
                map = (LinkedHashMap<String, String>) entry.getValue();
            }
        }

        return map;
    }

    public void addSuggestion(String key, String value) {
        suggestions.put(key, value);
    }

    public void addSuggestions(HashMap suggestions) {
        Iterator it = suggestions.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            this.suggestions.put(pair.getKey().toString(), pair.getValue().toString());
            it.remove(); // avoids a ConcurrentModificationException
        }
    }

    public HashMap<String, String> getSuggestions() {
        return suggestions;
    }
}
